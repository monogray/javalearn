package pageobject.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import tests.webdriver.old.Generator;
import utils.elements.*;

import java.util.HashMap;

public class BoardPage {

    private final WebDriver driver;
    private WebDriverWait wait;
    private Generator generator;
    private LocalizationProcessing local;

    private DynamicLocator createTaskByNameBt = new DynamicLocator("//h2[contains(text(),\"__listName__\")]/../../a[contains(text(),\"__createTaskBt__\")]", "xpath");
    private By cardNameFld = By.xpath("//div[@class=\"card-composer\"]//textarea");

    public BoardPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);

        this.generator = new Generator();
        this.local = new LocalizationProcessing();
    }

    public void createTask (String listName) {
        this.createTask(listName, "random", "");
    }

    public void createTask (String listName, String taskName) {
        this.createTask(listName, "notRandom", taskName);
    }

    private void createTask (String listName, String mode, String taskName) {
        HashMap<String, String> data = new HashMap<String, String>();
        data.put("listName", listName);
        data.put("createTaskBt", local.get("Ru", "Add card"));

        wait.until(ExpectedConditions.visibilityOfElementLocated(createTaskByNameBt.get(data)));
        WebElement newCard = this.driver.findElement(createTaskByNameBt.get(data));
        newCard.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(cardNameFld));
        WebElement cardTextArea = this.driver.findElement(cardNameFld);

        String newCardName;
        if (mode.equals("random")) {
            newCardName = "AT " + this.generator.givenUsingPlainJava_whenGeneratingRandomStringBounded_thenCorrect(10);
        } else {
            newCardName = taskName;
        }

        cardTextArea.sendKeys(newCardName);

        cardTextArea.sendKeys(Keys.ENTER);
    }

    public BoardPage checkPage (String boardName) {
        // TODO Implement this code
        System.out.println("Page header checked: " + boardName);

        return this;
    }
}
