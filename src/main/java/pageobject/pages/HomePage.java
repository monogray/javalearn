package pageobject.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import tests.webdriver.old.Generator;
import utils.elements.*;
import java.util.HashMap;

public class HomePage {

    private final WebDriver driver;
    private WebDriverWait wait;
    private Generator generator;
    private LocalizationProcessing local;

    private By pageHeader = By.xpath("//h3[@class=\"boards-page-board-section-header-name\"]");
    private DynamicLocator defaultBoardCard = new DynamicLocator("//a[@__attrName__=\"__className__\" and span[span[contains(text(),\"__boardName__\")]]]", "xpath");

    public HomePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);

        this.generator = new Generator();
        this.local = new LocalizationProcessing();
    }

    public BoardPage openBoard (String boardName) {
        HashMap data = new HashMap<String, String>();
        data.put("attrName", "class");
        data.put("className", "board-tile");
        data.put("boardName", boardName);

        WebElement desk = this.driver.findElement(this.defaultBoardCard.get(data));
        desk.click();

        return new BoardPage(this.driver);
    }

    public HomePage checkPage () {
        wait.until(ExpectedConditions.visibilityOfElementLocated(this.pageHeader));
        WebElement dashboard = this.driver.findElement(this.pageHeader);

        Assert.assertEquals("WRONG PAGE", dashboard.getText(), "Персональные доски");

        return this;
    }
}
