package pageobject.pages;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.openqa.selenium.*;
import tests.webdriver.AuthTest;
import tests.webdriver.old.Generator;
import utils.elements.LocalizationProcessing;

public class LoginPage {

    private final WebDriver driver;
    private WebDriverWait wait;
    private Generator generator;
    private LocalizationProcessing local;

    private By usernameLocator = By.id("user");
    private By passwordLocator = By.id("password");
    private By loginButtonLocator = By.id("???");

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 10);

        this.generator = new Generator();
        this.local = new LocalizationProcessing();
    }

    public LoginPage open() {
        this.driver.get("https://trello.com/login");

        WebElement loginField = this.driver.findElement(this.usernameLocator);
        this.wait.until(ExpectedConditions.visibilityOf(loginField));

        return this;
    }

    private LoginPage typeUsername(String username) {
        WebElement loginField = this.driver.findElement(this.usernameLocator);
        loginField.sendKeys(username);

        return this;
    }

    private LoginPage typePassword(String password) {
        WebElement passField = this.driver.findElement(this.passwordLocator);
        passField.sendKeys(password);

        return this;
    }

    private HomePage submitLogin() {
        WebElement passField = this.driver.findElement(this.passwordLocator);
        passField.submit();

        return new HomePage(driver);
    }

    public HomePage loginAs(String username, String password) {
        this.typePassword(password)
                .typeUsername(username)
                .submitLogin();

        return new HomePage(this.driver);
    }
}