package utils.elements;

import org.openqa.selenium.By;

import java.util.*;

public class DynamicLocator {
    private String locator;
    private String type;

    public DynamicLocator (String locator, String type) {
        this.locator = locator;
        this.type = type;
    }

    public By get (HashMap<String, String> data) {
        // TODO Реализовать код, который универсальным образом на основании пришедших данных (HashMap со структурой: Наименование переменной->ЕЕ значение)
        // будет генерировать соответствующие локаторы

        String locator = this.locator;
        for (Map.Entry<String, String> entry: data.entrySet()) {
            locator = locator.replaceAll("__" + entry.getKey() + "__", entry.getValue());
        }

        System.out.println(locator);

        if (this.type.equals("xpath")) {
            return By.xpath(locator);
        } else if (this.type.equals("id")) {
            return By.id(locator);
        }

        return null;
    }
}
