package utils.elements;

import java.util.HashMap;

public class LocalizationProcessing {

    private HashMap<String, String> dataRu = new HashMap<String, String>();
    private HashMap<String, String> dataDe = new HashMap<String, String>();
    private HashMap<String, String> dataFr = new HashMap<String, String>();
    private HashMap<String, String> dataHi = new HashMap<String, String>();

    public LocalizationProcessing () {
        this.dataRu.put("Add card",     "Добавить карточку");
        this.dataRu.put("Remove card",  "Удалить карточку");

        this.dataDe.put("Add card",     "DE Add card");
        this.dataDe.put("Remove card",  "DE Remove card");
    }

    public String get (String local, String string) {

        if (local.equals("Ru")) {
            return dataRu.get(string);
        } else if (local.equals("De")) {
            return dataDe.get(string);
        } else if (local.equals("Fr")) {
            return dataFr.get(string);
        }

        return null;
    }
}
