package tests.junit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.webdriver.AuthTest;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        JunitExample.class,
        AuthTest.class
})

public class FeatureTestSuite {
    // the class remains empty,
    // used only as a holder for the above annotations
}