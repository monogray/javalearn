package tests.junit;

import java.util.Arrays;
import java.util.List;

public class DataCombiner {
    public List<Object[]> getLogins () {
        //....
        //...
        //...

        return Arrays.asList(new Object[][] {
                {0, 1}, {10, 20}, {50, 60}, {0, 1}, {10, 20}, {50, 60}, {0, 1}, {10, 20}, {50, 60}
        });
    }
}
