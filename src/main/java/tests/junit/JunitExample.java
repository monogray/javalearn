package tests.junit;

import java.util.*;
import org.junit.*;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class JunitExample {

    @Parameters
    public static Collection<Object[]> data() {
        DataCombiner dat = new DataCombiner();
        return dat.getLogins();
    }

    private int dat1;
    private int dat2;

    public JunitExample(int x, int y) {
        this.dat1 = x;
        this.dat2 = y;
    }

    @BeforeClass
    public static void initGlobalResources() {
        /**
         * This method will be called only once per test class.
         */
        System.out.println("@BeforeClass");
    }

    @Before
    public void beforeTest() {
        System.out.println("@Before");

        System.out.println("Set data 'dat1 = " + this.dat1 + "'");
        System.out.println("Set data 'dat2 = " + this.dat2 + "'");
    }

    @After
    public void afterTest() {
        System.out.println("@After");
    }

    @Test
    public void simpleTest() {
        System.out.println("@Test");

        /**
         * Use Assert methods to call your methods to be tested.
         * A simple test to check whether the given list is empty or not.
         */
        Assert.assertTrue(new ArrayList().isEmpty());
    }

    @Test(timeout = 100)
    public void TimeoutTest() {
        System.out.println("@Test(timeout=100)");

        try {
            Thread.sleep(20);
        } catch (Exception e) {

        }

        /**
         * The IO operation has to be done with in 100 milli seconds. If not,
         * the test should fail.
         */

        // ....
        // ....
        // ....
    }

    @Ignore
    @Test
    public void IgnoreTestMethod() {
        /**
         * This test will be ignored.
         */

        System.out.println("@Ignore @Test");
    }

    @AfterClass
    public static void closeGlobalResources() {
        /**
         * This method will be called only once per test class. It will be called
         * after executing test.
         */
        System.out.println("@AfterClass");
    }

}
