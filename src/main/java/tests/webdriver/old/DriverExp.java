package tests.webdriver.old;

import org.junit.*;
import static org.junit.Assert.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.*;

public class DriverExp {

    private static WebDriver browser;

    @BeforeClass
    public static void init() {
        //Firefox's geckodriver *requires* you to specify its location.
        System.setProperty(
                "webdriver.gecko.driver",
                "/Users/user/Dropbox/Development/java-projects/Libraries/lib/selenium/geckodriver"
        );
        DriverExp.browser = new FirefoxDriver();
    }

    @Test
    public void testOne() {
        browser.get("http://saucelabs.com");
        WebElement header = browser.findElement(By.id("site-header"));

        assertTrue((header.isDisplayed()));
    }

    @Test
    public void testTwo() {
        browser.get("http://www.javacodegeeks.com/");
        WebElement element = browser.findElement(By.name("s"));
        element.sendKeys("selenuim");
        element.submit();
    }

    @AfterClass
    public static void close() {
        //tests.webdriver.old.DriverExp.browser.close();
    }

}