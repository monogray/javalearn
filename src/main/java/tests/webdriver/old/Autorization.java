package tests.webdriver.old;

import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Iterator;
import java.util.List;
import org.openqa.selenium.firefox.*;

public class Autorization {
    private static WebDriver browser;
    private static String baseUrl = "http://trello.com";
    private String deskLocator = "//a[@class=\"board-tile\" and span[span[contains(text(),\"Доска\")]]]";

    @Before
    public void init() {
        //Firefox's geckodriver *requires* you to specify its location.
        System.setProperty(
                "webdriver.gecko.driver",
                "/Users/user/Dropbox/Development/java-projects/Libraries/lib/selenium/geckodriver"
        );
        Autorization.browser = new FirefoxDriver();
    }


//    e.gubich+test@helsi.me
//            test123456789

    @Test
    public void testOne() {
        browser.get(Autorization.baseUrl);
        Generator generator = new Generator();
        WebDriverWait wait = new WebDriverWait(browser, 10);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//a[contains(@href,\"/login\")])[1]")));
        WebElement login = browser.findElement(By.xpath("(//a[contains(@href,\"/login\")])[1]"));
        login.click();

        WebElement loginFld = browser.findElement(By.id("user"));
        wait.until(ExpectedConditions.visibilityOf(loginFld));


        WebElement passwordFld = browser.findElement(By.id("password"));
        loginFld.sendKeys("e.gubich+test@helsi.me");
        wait.until(ExpectedConditions.visibilityOf(passwordFld));
        passwordFld.sendKeys("test123456789");
        passwordFld.submit();


        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@class=\"boards-page-board-section-header-name\"]")));
        WebElement dashboard = browser.findElement(By.xpath("//h3[@class=\"boards-page-board-section-header-name\"]"));
        String dashBoardText = dashboard.getText();
        Assert.assertEquals("WRONG PAGE", dashBoardText, "Персональные доски");

        WebElement desk = browser.findElement(By.xpath(deskLocator));
        desk.click();


        // Создание таски
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[contains(text(),\"Нужно сделать\")]/../../a[contains(text(),\"Добавить карточку\")]")));
        WebElement newCard = browser.findElement(By.xpath("//h2[contains(text(),\"Нужно сделать\")]/../../a[contains(text(),\"Добавить карточку\")]"));
        newCard.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class=\"card-composer\"]//textarea")));
        WebElement cardTextArea = browser.findElement(By.xpath("//div[@class=\"card-composer\"]//textarea"));


        String newCardname = "AT " + generator.givenUsingPlainJava_whenGeneratingRandomStringBounded_thenCorrect(10);
        cardTextArea.sendKeys(newCardname);
        cardTextArea.sendKeys(Keys.ENTER);
        // END Создание таски

        WebElement lastAddCard = browser.findElement(By.xpath("(//div[h2[contains(text(),\"Нужно сделать\")]]/..//span[contains(@class,\"list-card-title js-card-name\")])[last()]"));
        wait.until(ExpectedConditions.visibilityOf(lastAddCard));
        String lastAddCardText = lastAddCard.getText();
        Assert.assertEquals("No new card existed", lastAddCardText, newCardname);

        lastAddCard.click();
        String newDescriptionText = generator.givenUsingPlainJava_whenGeneratingRandomStringBounded_thenCorrect(50);
        WebElement descriptionTextarea = browser.findElement(By.xpath("//textarea[@class=\"field js-description-draft\"]"));
        descriptionTextarea.sendKeys(newDescriptionText);

        WebElement saveDescriptionBtn = browser.findElement(By.xpath("//div[contains(@class,\"card-detail-window\")]//input[contains(@class,\"js-save-edit\")]"));
        saveDescriptionBtn.click();

        WebElement savedDescriptionTextarea = browser.findElement(By.xpath("//div[contains(@class,\"js-show-with-desc\")]/p"));
        String savedDescriptionText = savedDescriptionTextarea.getText();
        Assert.assertEquals("No description text", savedDescriptionText, newDescriptionText);

        WebElement emojiIcon = browser.findElement(By.xpath("//div[contains(@class,\"comment-frame\")]//span[contains(@class,\"icon-sm icon-emoji\")]"));
        wait.until(ExpectedConditions.visibilityOf(emojiIcon));
        emojiIcon.click();

        WebElement emojiFilterInput = browser.findElement(By.xpath("//input[contains(@class,\"js-filter-emoji\")]"));
        wait.until(ExpectedConditions.visibilityOf(emojiFilterInput));

        String emoji = "smile";
        emojiFilterInput.sendKeys(emoji);
        emojiFilterInput.sendKeys(Keys.ENTER);

        WebElement saveCommentBtn = browser.findElement(By.xpath("//div[contains(@class,\"card-detail-window\")]//input[contains(@class,\"js-add-comment\")]"));
        saveCommentBtn.click();

        WebElement displayEmoji = browser.findElement(By.xpath("//div[contains(@class,\"card-detail-window\")]//img[@title=\"" + emoji + "\"]"));
        wait.until(ExpectedConditions.visibilityOf(displayEmoji));

    }

    @Test
    public void testTwo() {
        browser.get(Autorization.baseUrl);
        Generator generator = new Generator();
        WebDriverWait wait = new WebDriverWait(browser, 10);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//a[contains(@href,\"/login\")])[1]")));
        WebElement login = browser.findElement(By.xpath("(//a[contains(@href,\"/login\")])[1]"));
        login.click();

        WebElement loginFld = browser.findElement(By.id("user"));
        wait.until(ExpectedConditions.visibilityOf(loginFld));


        WebElement passwordFld = browser.findElement(By.id("password"));
        loginFld.sendKeys("e.gubich+test@helsi.me");
        wait.until(ExpectedConditions.visibilityOf(passwordFld));
        passwordFld.sendKeys("test123456789");
        passwordFld.submit();


        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@class=\"boards-page-board-section-header-name\"]")));
        WebElement dashboard = browser.findElement(By.xpath("//h3[@class=\"boards-page-board-section-header-name\"]"));
        String dashBoardText = dashboard.getText();
        Assert.assertEquals("WRONG PAGE", dashBoardText, "Персональные доски");

        WebElement desk = browser.findElement(By.xpath(deskLocator));
        desk.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[contains(text(),\"Нужно сделать\")]/../../a[contains(text(),\"Добавить карточку\")]")));
        WebElement newCard = browser.findElement(By.xpath("//h2[contains(text(),\"Нужно сделать\")]/../../a[contains(text(),\"Добавить карточку\")]"));
        newCard.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class=\"card-composer\"]//textarea")));
        WebElement cardTextArea = browser.findElement(By.xpath("//div[@class=\"card-composer\"]//textarea"));

        String newCardname = "AT " + generator.givenUsingPlainJava_whenGeneratingRandomStringBounded_thenCorrect(10);
        cardTextArea.sendKeys(newCardname);
        cardTextArea.sendKeys(Keys.ENTER);

        WebElement lastAddCard = browser.findElement(By.xpath("(//div[h2[contains(text(),\"Нужно сделать\")]]/..//span[contains(@class,\"list-card-title js-card-name\")])[last()]"));
        wait.until(ExpectedConditions.visibilityOf(lastAddCard));
        String lastAddCardText = lastAddCard.getText();
        Assert.assertEquals("No new card existed", lastAddCardText, newCardname);

//TODO не сробатывает проверка =(
        WebElement inprogressList = browser.findElement(By.xpath("//h2[contains(text(),\"В процессе\")]/../.."));
        List<WebElement> searchArticle1 = browser.findElements(By.xpath("//h2[contains(text(),\"В процессе\")]/../..//a[contains(@class,\"list-card\")]"));
        int beforDragAndDrop = searchArticle1.size();
        new Actions(browser).dragAndDrop(lastAddCard, inprogressList).perform();

        List<WebElement> searchArticle2 = browser.findElements(By.xpath("//h2[contains(text(),\"В процессе\")]/../..//a[contains(@class,\"list-card\")]"));
        int afterDragAndDrop = searchArticle2.size();
        Iterator iterator = searchArticle2.iterator();

        while (iterator.hasNext() && afterDragAndDrop > beforDragAndDrop) {
            WebElement element = (WebElement) iterator.next();
//         System.out.println(element.getText());
            Assert.assertTrue("There now ticket name: \"" + lastAddCardText + "\" in search result", element.getText().toLowerCase().contains(lastAddCardText));
        }

    }

    @After
    public void close() {
        Autorization.browser.close();
    }
}