package tests.webdriver.old;

import org.openqa.selenium.*;
import org.openqa.selenium.remote.*;

import java.net.MalformedURLException;
import java.net.URL;

/*
java -Dwebdriver.gecko.driver=/Users/user/Dropbox/Development/java-projects/Libraries/lib/selenium/geckodrive -Dwebdriver.chrome.driver=/Users/user/Dropbox/Development/java-projects/Libraries/lib/selenium/chromedriver -jar /Users/user/Dropbox/Development/java-projects/Libraries/lib/selenium/selenium-server-standalone-3.8.1.jar
 */

public class DriverStandaloneExp {

    public static void main(String[] args) throws MalformedURLException {

        WebDriver driver = new RemoteWebDriver(
                new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.chrome()
        );
        /*WebDriver driver = new RemoteWebDriver(
                new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.chrome()
        );*/

        driver.get("http://www.javacodegeeks.com/");
        WebElement element = driver.findElement(By.name("s"));
        element.sendKeys("selenuim");
        element.submit();

        driver.quit();
    }

}