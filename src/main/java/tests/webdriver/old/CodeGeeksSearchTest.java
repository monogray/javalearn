package tests.webdriver.old;

import org.junit.*;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.*;

import java.util.*;

public class CodeGeeksSearchTest {

    private static WebDriver browser;
    private static String baseUrl = "http://www.javacodegeeks.com/";


    @BeforeClass
    public static void init() {
        //Firefox's geckodriver *requires* you to specify its location.
        System.setProperty(
                "webdriver.gecko.driver",
                "/Users/user/Dropbox/Development/java-projects/Libraries/lib/selenium/geckodriver"
        );
        CodeGeeksSearchTest.browser = new FirefoxDriver();
    }

    @Test
    public void testTwo() {
        browser.get(CodeGeeksSearchTest.baseUrl);

        WebDriverWait wait = new WebDriverWait(browser, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//div[@data-appearance=\"bounceInDown\"])[1]")));

        WebElement header = browser.findElement(By.xpath("(//div[@data-appearance=\"bounceInDown\"])[1]"));
        Assert.assertEquals("Do you want to know how to develop your skillset to become a Java Rockstar?", header.getText());

        WebElement searchField = browser.findElement(By.id("s"));
        searchField.sendKeys("selenium");
        searchField.submit();

        wait = new WebDriverWait(browser, 5);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[@class=\"page-title\"]")));

        WebElement searchResultsHeader = browser.findElement(By.xpath("//h2[@class=\"page-title\"]"));
        Assert.assertEquals("Search Results for: selenium", searchResultsHeader.getText());

        WebElement searchResultsPageCount = browser.findElement(By.xpath("//span[@class=\"pages\"]"));

        String[] pageCountArr = searchResultsPageCount.getText().split(" ");
        Integer lastPageCount = Integer.valueOf(pageCountArr[pageCountArr.length - 1]);

        List<WebElement> searchArticles = browser.findElements(By.xpath("//div[contains(@class, \"post-listing\")]//h2"));
        Iterator iterator = searchArticles.iterator();
        while (iterator.hasNext()) {
            WebElement element = (WebElement) iterator.next();
            Assert.assertTrue("There are no 'selenium' in search results!", element.getText().toLowerCase().contains("selenium"));
        }

        WebElement lastPageBt = browser.findElement(By.xpath("//*[@class='last']"));
        lastPageBt.click();

        WebElement lastCurrentPage = browser.findElement(By.xpath("//div[@class='pagination']/span[@class='current']"));
        Integer lastPageNumber = Integer.valueOf(lastCurrentPage.getText());

        Assert.assertEquals("Page numbers is not valid", lastPageCount, lastPageNumber);

    }

    @AfterClass
    public static void close() {
        //tests.webdriver.old.CodeGeeksSearchTest.browser.close();
    }

}