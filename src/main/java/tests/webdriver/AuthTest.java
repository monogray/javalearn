package tests.webdriver;

import org.junit.*;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.*;
import pageobject.pages.*;

public class AuthTest {

    private static WebDriver browser;
    private static String baseUrl = "https://trello.com/";
    private static String local = "Ru";

    @BeforeClass
    public static void init() {
        //Firefox's geckodriver *requires* you to specify its location.
        System.setProperty(
                "webdriver.gecko.driver",
                "/Users/user/Dropbox/Development/java-projects/Libraries/lib/selenium/geckodriver"
        );
        AuthTest.browser = new FirefoxDriver();
    }

    @Test
    public void cardCreationTest() {
        LoginPage loginPage = new LoginPage(AuthTest.browser);

        HomePage homePage = loginPage.open().loginAs("e.gubich+test@helsi.me", "test123456789");
        BoardPage boardPage = homePage.checkPage().openBoard("Доска без названия");

        boardPage.checkPage("Доска без названия").createTask("Нужно сделать");
    }











    /*@Test
    public void authorizationTest() {
        AuthTest.browser.get(AuthTest.baseUrl);

        WebDriverWait wait = new WebDriverWait(browser, 10);

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("(//div[contains(@class, 'global-header-section')])[2]/a[contains(text(), 'Войти')]")));
        WebElement loginBt = browser.findElement(By.xpath("(//div[contains(@class, 'global-header-section')])[2]/a[contains(text(), 'Войти')]"));

        loginBt.click();

        WebElement loginField = browser.findElement(By.id("user"));
        wait.until(ExpectedConditions.visibilityOf(loginField));
        loginField.sendKeys("e.gubich+test@helsi.me");

        WebElement passField = browser.findElement(By.id("password"));
        passField.sendKeys("test123456789");
        passField.submit();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[@class='boards-page-board-section-header-name']")));
        WebElement dashboardHeader = browser.findElement(By.xpath("//h3[@class='boards-page-board-section-header-name']"));
        Assert.assertEquals("Персональные доски", dashboardHeader.getText());

        // TODO. Refactor xpath
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(AuthTest.locators.defaultBoardLocator)));
        WebElement defaultBoard = browser.findElement(By.xpath(AuthTest.locators.defaultBoardLocator));
        defaultBoard.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[contains(text(), 'Нужно сделать')]/../../a[contains(text(), 'Добавить карточку')]")));
        WebElement ToDoRow = browser.findElement(By.xpath("//h2[contains(text(), 'Нужно сделать')]/../../a[contains(text(), 'Добавить карточку')]"));
        ToDoRow.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class=\"card-composer\"]//textarea")));
        WebElement cardTextarea = browser.findElement(By.xpath("//div[@class=\"card-composer\"]//textarea"));

        cardTextarea.sendKeys("Auto test new card");
        cardTextarea.sendKeys(Keys.ENTER);

    }*/

    @AfterClass
    public static void close() {
        // AuthTest.browser.close();
    }

}